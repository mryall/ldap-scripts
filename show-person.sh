#!/bin/bash

PERSON="$1"
if [ -z "$PERSON" ]; then
    PERSON=mryall
fi
ldapsearch -xLLL -D 'uid=mryall,ou=people,dc=atlassian,dc=com' -W -H 'ldaps://ldap.atlassian.com' -b "uid=$PERSON,ou=people,dc=atlassian,dc=com" -s base '(objectClass=*)'
