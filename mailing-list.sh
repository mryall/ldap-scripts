#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 list"
    exit 1
fi
ldapsearch -xLLL -D 'uid=mryall,ou=people,dc=atlassian,dc=com' -W -H 'ldaps://ldap.atlassian.com' -b "mail=$1@atlassian.com,ou=Groups,ou=GoogleAppsGroups,dc=atlassian,dc=com"

