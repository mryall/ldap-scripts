#!/bin/bash

ATTR="$1"
if [ -z "$ATTR" ]; then
    ATTR=mail
fi
ldapsearch -xLLL -D 'uid=mryall,ou=people,dc=atlassian,dc=com' -W -H 'ldaps://ldap.atlassian.com' -b 'ou=people,dc=atlassian,dc=com' '(&(objectClass=inetOrgPerson)(businessCategory=R&D:Confluence Engineering)(accountActive=TRUE)(!(l=Vietnam)))' "$ATTR" | grep "$ATTR" | awk '{ $1 = ""; print $0 }' | sed 's/^ *//' | sort
