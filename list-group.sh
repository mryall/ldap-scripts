#!/bin/bash

GROUP="$1"
if [ -z "$GROUP" ]; then
    GROUP=confluence-dev
fi
ldapsearch -xLLL -D 'uid=mryall,ou=people,dc=atlassian,dc=com' -W -H 'ldaps://ldap.atlassian.com' -b "cn=$GROUP,ou=Groups,dc=atlassian,dc=com" -s base '(objectClass=*)' memberUid 
